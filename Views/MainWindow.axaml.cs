using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Avalonia.Media;
using Avalonia.Threading;

namespace Tarefa4.Views;

public partial class MainWindow : Window
{
    private readonly Image? _charmander;
    private readonly Image? _charmanderWalk;
    private readonly Image? _charmanderJump;
    private readonly Image? _charmanderDuck;
    private readonly Image? _charmanderPlayer;
    private double _charmanderlLeft;
    private double _charmanderBottom;
    private DispatcherTimer _timer;
    private int _walkCounter = 0;



    public MainWindow()
    {
        InitializeComponent();
#if DEBUG
        this.AttachDevTools();
#endif
        var pokemonGame = this.FindControl<Canvas>("PokemonGame");
        _charmander = this.FindControl<Image>("Charmander");
        _charmanderWalk = this.FindControl<Image>("CharmanderWalk");
        _charmanderJump = this.FindControl<Image>("CharmanderJump");
        _charmanderDuck = this.FindControl<Image>("CharmanderDuck");
        _charmanderlLeft = 0;
        _charmanderBottom = 60;
        
        _charmanderPlayer = new Image
        {
            Width = 80,
            Source = _charmander!.Source,
            IsVisible = true
        };
        pokemonGame!.Children.Add(_charmanderPlayer);
        Canvas.SetBottom(_charmanderPlayer,60);
        
        
        KeyDown += KeyPressed;

        DataContext = this;
        PlayAudio("sound.mp3", 0.1f, true);
        
        _timer = new DispatcherTimer();
        _timer.Interval = TimeSpan.FromMilliseconds(5);
        _timer.Tick += TimerTick;
        _timer.Start();

    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }

    private void KeyPressed(object? sender, KeyEventArgs e)
    {
        const double moveStep = 30.0;

        switch (e.Key)
        {
            case Key.Left or Key.A:
                MoveLeft(moveStep);
                break;
            case Key.Right or Key.D:
                MoveRight(moveStep);
                break;
            case Key.Space or Key.W or Key.Up:
                JumpControl();
                PlayAudio("jump.wav", 0.1f, false);
                break;
            case Key.Down or Key.S:
                DuckControl();
                PlayAudio("duck.wav", 0.1f, false);
                break;
        }
    }

    private void MoveLeft(double offset)
    {

        if ((_charmanderlLeft - offset) >= 0)
        {
            _charmanderlLeft -= offset;
            Canvas.SetLeft(_charmanderPlayer!, _charmanderlLeft);
        }

        _charmanderPlayer!.Source = (_walkCounter % 2 == 0) ? _charmander!.Source : _charmanderWalk!.Source;
        _walkCounter++;
        
        // Inverter a imagem
        _charmanderPlayer!.RenderTransformOrigin = new RelativePoint(0.5, 0.5, RelativeUnit.Relative);
        _charmanderPlayer.RenderTransform = new ScaleTransform { ScaleX = -1 };
    }
    
    private void MoveRight(double offset)
    {

        if ((_charmanderlLeft + offset) <= (1280 - _charmanderPlayer!.Width))
        {
            _charmanderlLeft += offset;
            Canvas.SetLeft(_charmanderPlayer!, _charmanderlLeft);
        }
        _charmanderPlayer!.Source = (_walkCounter % 2 == 0) ? _charmander!.Source : _charmanderWalk!.Source;
        _walkCounter++;
        
        _charmanderPlayer!.RenderTransform = new ScaleTransform { ScaleX = 1 };

        
    }
    
    private void JumpControl()
    {
        if (_charmanderBottom < 600)
        {
            _charmanderBottom += 50;
            Canvas.SetBottom(_charmanderPlayer!, _charmanderBottom);

            _charmanderPlayer!.Source = _charmanderJump!.Source;
        }
       
    }
    
    private void DuckControl()
    {
        _charmanderPlayer!.Source = _charmanderDuck!.Source;
    }
    
    private void TimerTick(object? sender, EventArgs e)
    {
        if (_charmanderBottom > 60)
        {
            _charmanderBottom -= 10;
            Canvas.SetBottom(_charmanderPlayer!, _charmanderBottom);
        }
    }
}