﻿using System;
using System.IO;
using Avalonia.Controls;
using Avalonia.Platform;
using NAudio.Wave;

namespace Tarefa4.Views;

public partial class MainWindow: Window
{
    private IWavePlayer _wavePlayer = null!;
    private AudioFileReader _audioFileReader = null!;
    private bool _loop = false;

    private void PlayAudio(string assetName, float volume, bool loop)
    {
        using var stream = AssetLoader.Open(new Uri($"avares://Tarefa4/Assets/{assetName}"));
        if (stream == null)
            throw new InvalidOperationException("Resource not found.");

        var tempFile = Path.GetTempFileName();
        using (var fileStream = File.Create(tempFile))
        {
            stream.CopyTo(fileStream);
        }
            
        _wavePlayer = new WaveOutEvent();
        _audioFileReader = new AudioFileReader(tempFile);
        _wavePlayer.Init(_audioFileReader);
        _wavePlayer.Volume = volume;
        _loop = loop;

        _wavePlayer.PlaybackStopped += (_, _) =>
        {
            if (_loop)
            {
                _audioFileReader.Position = 0; // Reinicia a posição do leitor de áudio
                _wavePlayer.Play(); // Reinicia a reprodução
            }
            else
            {
                _audioFileReader.Dispose();
                _wavePlayer.Dispose();
                File.Delete(tempFile);
            }
        };

        _wavePlayer.Play();
    }
}